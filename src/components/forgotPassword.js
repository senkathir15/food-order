import React from 'react'
import { Form,Button,Card,Alert } from 'react-bootstrap'
import { useRef ,useContext,useState} from 'react';
import {AuthContext} from '../context/authContext';
import {Link,useHistory} from "react-router-dom";
export default function ForgotPassWord() {

   const emailRef = useRef();
   const {resetPassword}= useContext(AuthContext);
   const [error,setError]=useState("");

   async function handelSubmit(e) {
     e.preventDefault();
     try {
       await resetPassword(emailRef.current.value);
       setError("check your inbox for further ");
     } catch {
       setError("Failed to reset");
       setInterval(() => {
         setError("");
       }, 2000);
     }
   }

    return (
      <>
        <Card  className=" w-60 m-auto">
          <Card.Body>
            <h2 className="text-center mb-4">ForgotPassWord</h2>
            {error && <Alert>{error}</Alert>}
            <Form onSubmit={handelSubmit}>
              <Form.Group className="mb-3" id="Email">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email"  ref={emailRef} placeholder="Enter email" />
              </Form.Group>
              <Button  type="submit" className="w-100" variant="primary" type="submit">Reset</Button>
            </Form>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
           <Link to="/login">Back to login</Link>
        </div>
        <div className="w-100 text-center mt-2">
            Don't have account? <Link to="/signup">signup</Link>
        </div>
      </>
    );
}