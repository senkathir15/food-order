import React from 'react'
import { Form,Button,Card,Alert } from 'react-bootstrap'
import { useRef ,useContext,useState} from 'react';
import {AuthContext} from '../context/authContext';
import { Link,useHistory} from 'react-router-dom';

export default function SignUp() {
   const emailRef = useRef();
   const passWordRef = useRef();
   const conformPassRef = useRef();
   const { signup,currentUser }= useContext(AuthContext);
   const [error,setError]=useState("");
   const history = useHistory();

   async function handelSubmit (e){
       e.preventDefault();
      
       if(passWordRef.current.value !== conformPassRef.current.value){
        setError("passWord does not match")
        setInterval(()=>{
            setError("");
        },2000)
        return ;
       }
       try {
           setError("");
        await signup(emailRef.current.value,passWordRef.current.value);
           history.push("/login");
       }
       catch{
           setError("sign up Failed")

       }



   }

    return (
      <>
        <Card  className=" w-60 m-auto">
          <Card.Body>
            <h2 className="text-center mb-4">Sign-up</h2>
            {error && <Alert>{error}</Alert>}
            <Form onSubmit={handelSubmit}>
              <Form.Group className="mb-3" id="Email">
                <Form.Label>Email address</Form.Label>
                <Form.Control type="email"  ref={emailRef} placeholder="Enter email" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="passWord">
                <Form.Label>Password</Form.Label>
                <Form.Control type="password"  ref={passWordRef} placeholder="Password" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="confirmPassWord">
                <Form.Label>Password Conformation</Form.Label>
                <Form.Control type="password" ref={conformPassRef} placeholder="conformPassWord" />
              </Form.Group>
              <Button  type="submit" className="w-100" variant="primary" type="submit">signUp</Button>
            </Form>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
           Already have an account? <Link to="/login">Login</Link>
        </div>
      </>
    );
}
