import React from "react"
import { Route, Redirect } from "react-router-dom"
import { AuthContext } from "../context/authContext";

export default function PrivateRoute({ component: Component, ...rest }) {
  const { currentUser } = React.useContext(AuthContext)

  return (
    <Route
      {...rest}
      render={props => {
        return currentUser ? <Component {...props} /> : <Redirect to="/login" />
      }}
    ></Route>
  )
}