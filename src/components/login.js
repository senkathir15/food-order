import React from 'react'
import { Form,Button,Card,Alert } from 'react-bootstrap'
import { useRef ,useContext,useState} from 'react';
import {AuthContext} from '../context/authContext';
import {Link,useHistory} from "react-router-dom";

export default function Login() {
   const emailRef = useRef();
   const passWordRef = useRef();
   const history = useHistory();
   const {login}= useContext(AuthContext);
   const [error,setError]=useState("");

   async function handelSubmit (e){
       e.preventDefault();
       try {
           
        await login(emailRef.current.value,passWordRef.current.value);
           history.push("/");
       }
       catch{
           setError("Account Does't Exist");
           setInterval(()=>{
            setError("");
        },2000)
    

       }



   }

    return (
      <>
        <Card  className=" w-60 m-auto">
          <Card.Body>
            <h2 className="text-center mb-4">Login</h2>
            {error && <Alert>{error}</Alert>}
            <Form onSubmit={handelSubmit}>
                <Form.Group className="mb-3" id="Email">
                  <Form.Label>Email address</Form.Label>
                  <Form.Control type="email"  ref={emailRef} placeholder="Enter email" />
                </Form.Group>
                <Form.Group className="mb-3" controlId="passWord">
                  <Form.Label>Password</Form.Label>
                  <Form.Control type="password"  ref={passWordRef} placeholder="Password" />
                </Form.Group>
                <Button  type="submit" className="w-100" variant="primary" type="submit"> Login</Button>
            </Form>
            <div className="w-100 text-center mt-2">
                 <Link to="/forgotPassWord">Forgot passsWord ?</Link>
            </div>
          </Card.Body>
        </Card>
        <div className="w-100 text-center mt-2">
            Don't have account? <Link to="/signup">signup</Link>
        </div>
      </>
    );
}