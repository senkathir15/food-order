import React from 'react'
import { Form,Button,Card,Alert } from 'react-bootstrap'
import { useRef ,useContext,useState} from 'react';
import {AuthContext} from '../context/authContext';
import { Link,useHistory} from 'react-router-dom';
import NavBar from "../Dashboard components/nav"
import { Provider } from '../Dashboard components/context';
import Product from "../Dashboard components/product"
import Footer from "../Dashboard components/footer";

export default function SignUp() {
   
   const {currentUser,logout }= useContext(AuthContext);
   const [error,setError]=useState("");
   const history = useHistory();

   async function handelSubmit (){
       try {
           setError("");
         await logout();
           history.push("/login");
       }
       catch{
           setError("Logout up Failed")
       }
   }

    return (
      <>
        <Provider>
          <NavBar></NavBar>
          <Footer />
          <Product />
        </Provider>
      </>
    );
}
