
import React, { useState, useEffect,useReducer } from 'react';
import { DUMMY_MEALS } from './data';
const Context = React.createContext();
const defaultSate = {
    item:DUMMY_MEALS
  }
  const itemReducer = (state,action)=>{
    if(action.type === "filter"){
        let flterItem;
        if(action.payload === "All"){
            flterItem= DUMMY_MEALS;
        }
        else{
            flterItem = DUMMY_MEALS.filter(item => item.dish == action.payload )
        }
       return {
        item: flterItem
       }
        
    }

    if(action.type === "search"){
        let flterItem;
         flterItem =  DUMMY_MEALS.filter(item => {
            return item.name.toLowerCase().indexOf(action.payload.toLowerCase()) !== -1;
          });

          return {
            item: flterItem
           }
    }
    return defaultSate;
  }

const Provider = (prop)=>{
    const [state,dispatch]=useReducer(itemReducer,defaultSate);
    const filterItem =(id)=>{
        dispatch({type:"filter", payload:id})
    }
    const searchItem =(name)=>{
        dispatch({type:"search", payload:name})
    }
    const context = {
        item:state.item,
        filterItem,
        searchItem
    }

    return(<Context.Provider value={context}>
        {prop.children}
    </Context.Provider>)
    
} 

export {Context,Provider};
