import React from 'react'
import styled from 'styled-components';
import { FcSearch } from "react-icons/fc";
import { useState ,useEffect,useContext} from 'react';
import { Link,useHistory} from 'react-router-dom';
import { BsFillPersonFill,BsBoxArrowInRight } from "react-icons/bs";
import Tooltip from 'react-bootstrap/Tooltip'
import Overlay from 'react-bootstrap/Overlay'
import Nav from 'react-bootstrap/Nav'
import FormControl from 'react-bootstrap/FormControl';
import NavDropdown from 'react-bootstrap/NavDropdown';
import { GrShop } from "react-icons/gr"


import { Context } from './context';
import { Form,Button,Card,Alert } from 'react-bootstrap'
import {AuthContext} from '../context/authContext';
import CartContext from "../cartComponents/store/cart-context";
import Cart from "../cartComponents/Cart/cart"

export default function NavBar() {
  const [show,setShow]=useState(false);
  const [search,setsearch]=useState("");
  const {searchItem}= useContext(Context);
  const {currentUser,logout }= useContext(AuthContext);
   const [error,setError]=useState("");
   const history = useHistory();

   async function handelSubmit() {
     try {
       setError("");
       await logout();
       history.push("/login");
     } catch {
       setError("Logout up Failed");
     }
   }


    const hideAndshow = ()=>{
        setShow(!show)
    }

    const handlesearch=()=>{
      searchItem(search);
    }

    useEffect(() => {
      handlesearch();
    }, [search]);

    const [shows, setShows] = useState(false);
    const handleClose = () => setShows(false);
    const handleShow = () => setShows(true);
    const cartContext = useContext(CartContext);
    const numberOfItem = cartContext.items.reduce((currnum, item) => {
      return Number(currnum) + Number(item.amount);
    }, 0);

    
    return (
      <>
        <Navs>
        <Cart  show={shows} action={handleClose} />
            <h1>food order</h1>
            <form className="form">
                 <FcSearch className="search" onClick={hideAndshow}/>
                {show && <input className="input" type="search" placeholder="Search" onChange={(e)=>setsearch(e.target.value)} value={search}/>}
            </form>
            <div>
              <button className="btn" onClick={handleShow }>Your Cart<span>{numberOfItem}</span></button>
            </div>
            
            <NavDropdown title={<BsFillPersonFill/>} id="nav-dropdown" className="dropdown">
                <NavDropdown.Item eventKey="4.1">{currentUser.email}</NavDropdown.Item>
                <NavDropdown.Divider />
                <NavDropdown.Item eventKey="4.4"  onClick={handelSubmit}>LogOut</NavDropdown.Item>
            </NavDropdown>
              <div className="mcart" onClick={handleShow}><GrShop/>
                   <span>{numberOfItem}</span>
              </div>
        </Navs>
        </>
    )
}





const Navs = styled.nav`

  color: palevioletred;
  display:flex;
  align-items:center;
  justify-content:space-around;
  padding:0.5rem 2rem;
  background:yellow;
  position:sticky;
  box-sizing:border-box;
  h1{
    padding:10px;
    font-size:1.5rem;
  }
  @media (max-width: 450px) {
    padding:0 1rem;
   h1{
    font-size:1rem;
    color:green
    letter-spacing: 0.1rem;
    padding:5px;

   }
  }
 .mcart {
   display:none;
   @media (max-width: 650px) {
    display:flex;
  }
 }
  .form{
    display:flex;
      text-align:center;
      width:50%;
      padding-left: 1rem;
      @media (min-width: 500px) {
        width:30%;
      }
  }

  .icon{
      display:inline;
  }
  .search {
    font-size:1.5rem;
    
    padding:2px;
    
  }
  .user{
    text-align:center;
   width:10%;
   overflow: hidden;
  }
  p{
    font-size:1rem;
  }
  .input {
    border-color: block;
    outline-color: green;
    letter-spacing: 0.1rem;
    border-radius:20px;
    border:none;
    outline:none;
    color: block;
    padding: 0.25rem 0.5rem;
      width:100%;
      @media (max-width: 450px) {
        padding: 0.1rem 0.2rem;
        height:20px;
        border-radius:0px;

     }
  }

  button {
    display:flex;
    border-radius: 5px;
    border-color: transparent;
    padding: 0.25rem 0.5rem;
    text-transform: capitalize;
    letter-spacing: 0.2rem;
    background: blue;
    color: white;
    cursor: pointer;
    &:hover {
      background: white;
      color: blue;
      
    }

    @media (max-width: 650px) {
      padding: 0.1rem 0.2rem;
      letter-spacing: 0.1rem;
      display:none;
    }
  }
  span{
      color:red;
      background:white;
      font-size:10px;
      padding:4px;
      margin-left:5px;
      display:flex;
      border-radius: 5px;
      border:1px solid red;
      text-align:center;
  }
  .logout{
    display:none;
    @media (max-width: 450px) {
      display:flex;
         color:blue;
         font-size:1.5rem;   
    }
  }
`;