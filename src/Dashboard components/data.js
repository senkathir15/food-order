export const DUMMY_MEALS = [
    {
      id: 'm1',
      name: 'Sushi',
      img:"https://images.pexels.com/photos/704971/pexels-photo-704971.jpeg?cs=srgb&dl=pexels-daria-shevtsova-704971.jpg&fm=jpg",
      description: 'Finest fish and veggies',
      price: 22.99,
      dish:"meat"
    },
    {
      id: 'm2',
      name: 'sweet drink',
      img:"https://images.pexels.com/photos/1640777/pexels-photo-1640777.jpeg?cs=srgb&dl=pexels-ella-olsson-1640777.jpg&fm=jpg",
      description: 'A german specialty!',
      price: 16.5,
      dish:"meat"
    },
    {
      id: 'm3',
      name: 'Grill Fish',
      img:"https://images.pexels.com/photos/2233733/pexels-photo-2233733.jpeg?cs=srgb&dl=pexels-samer-daboul-2233733.jpg&fm=jpg",
      description: 'American, raw, meaty',
      price: 12.99,
      dish:"fish"
    },
    {
      id: 'm4',
      name: 'red Bowl',
      img:"https://images.pexels.com/photos/1583884/pexels-photo-1583884.jpeg?cs=srgb&dl=pexels-dzenina-lukac-1583884.jpg&fm=jpg",
      description: 'Healthy...and green...',
      price: 18.99,
      dish:"vegetables"
    },
    {
      id: 'm5',
      name: 'Green Bowl',
      img:"https://images.pexels.com/photos/1583884/pexels-photo-1583884.jpeg?cs=srgb&dl=pexels-dzenina-lukac-1583884.jpg&fm=jpg",
      description: 'Healthy...and green...',
      price: 18.99,
      dish:"vegetables"
    },
    {
      id: 'm6',
      name: 'Barbecue Fish',
      img:"https://images.pexels.com/photos/725991/pexels-photo-725991.jpeg?cs=srgb&dl=pexels-dana-tentis-725991.jpg&fm=jpg",
      description: 'American, raw, meaty',
      price: 12.99,
      dish:"fish"
    },
    {
      id: 'm7',
      name: 'Barbecue Burger',
      img:"https://images.pexels.com/photos/1092730/pexels-photo-1092730.jpeg?cs=srgb&dl=pexels-trang-doan-1092730.jpg&fm=jpg",
      description: 'American, raw, meaty',
      price: 12.99,
      dish:"meat"
    },
    {
      id: 'm8',
      name: 'vegetables Burger',
      img:"https://images.pexels.com/photos/1092730/pexels-photo-1092730.jpeg?cs=srgb&dl=pexels-trang-doan-1092730.jpg&fm=jpg",
      description: 'American, raw, meaty',
      price: 12.99,
      dish:"vegetables"
    },
    {
      id: 'm9',
      name: 'pran Burger',
      img:"https://images.pexels.com/photos/3843225/pexels-photo-3843225.jpeg?cs=srgb&dl=pexels-marianna-3843225.jpg&fm=jpg",
      description: 'American, raw, meaty',
      price: 12.99,
      dish:"fish"
    },

  ];