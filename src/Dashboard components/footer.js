import React from 'react'
import styled from 'styled-components';
import { Context } from './context';

const categoery = [
    {name:"All",id:"m1"},
    {name:"vegetables",id:"m1"},
    {name:"fish",id:"m2"},
     {name:"meat",id:"m3"},]

export default function Footer() {
    const {filterItem}=React.useContext(Context)
    return (
        <Section>
            {categoery.map((item)=>{
                const handleFilter = () => {
                  filterItem(item.name);
                };
                return(
                    <div className="categrey" id={item.id} onClick={()=>{handleFilter()}}>{item.name}</div>
                )
            })}
        </Section>
    )
}
const Section = styled.footer`

width:100%;
display:flex;
justify-content: space-around;
background:gray;
padding:10px 0;

h1{
    text-align:center;
}
.products{

    padding:0 100px;
}
.categrey:hover{
    color:white;
    border-bottom:4px solid blue;
}
  
`;