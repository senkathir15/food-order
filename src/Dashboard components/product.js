import React from 'react';
import styled from 'styled-components';
import ProductItem from './productItem';


export default function Product() {
  return (
    <Section>
      <h1>Our Products</h1>
      <div className="products">
        <ProductItem />
      </div>
    </Section>
  );
}

const Section = styled.section`

width:100%;

h1{
    text-align:center;
}
.products{

    padding:0 100px;
    @media (max-width: 450px) {
        padding:0 50px;
    }
}
`;