import React from 'react';
import styled from 'styled-components';
import { Context } from './context';
import CartContext from "../cartComponents/store/cart-context"

export default function ProductItem() {
  const { item } = React.useContext(Context);
  const { addItem } = React.useContext(CartContext);

  return (
    <Section>
      {item.length > 0 || <h1>No Result found</h1>}
      {item.map((item) => {
        const hadleAddCart = () => {
          addItem({
            id: item.id,
            name: item.name,
            amount: 1,
            price: item.price,
          });
        };

        return (
          <div className="card">
            <img src={item.img}></img>
            <div>
              <p>{item.name}</p>
              <h3>${item.price}</h3>
              <div>Description:</div>
              <p>{item.description}</p>
              <button onClick={hadleAddCart}>Add To Cart</button>
            </div>
          </div>
        );
      })}
    </Section>
  );
}
const Section = styled.div`

width:100%;
display:flex;
flex-wrap: wrap;
max-width:1000px;
margin:0 auto;

.card{
dispaly:flex;
max-width: 15rem;
margin: 2rem auto;
background-color: #f3e9e9;
color: rgb(252, 71, 71);
border-radius: 14px;
padding: 1rem;
box-shadow: 0 1px 18px 10px rgba(0, 0, 0, 0.25);

img {
    width:100%;
    height:200px;
    display:block;
    object-fit:contain;
}
p{
    text-align:center;
    color:black;
    margin:0;
}
h3{
    margin:0;
}
button{
   width:100%;
   color:white;
   border:1px solid green;
   background:blue;
   border-radius:10px;
   padding:10px;
   font-size:1rem;
   font-weight:bold;
}
h1{
    text-align:center;
    color:red;
    margin: 0 auto;
}
  
`;