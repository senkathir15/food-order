import SignUp from './components/signUp';
import { AuthProvider } from './context/authContext';
import {BrowserRouter as Router,Switch,Route } from "react-router-dom"
 import Login from './components/login';
 import Dhasborad from './components/dhasborad';
import ForgotPassWord from './components/forgotPassword'
import PrivateRoute from './components/privateRoute';
import UpdateProfile from './components/updateProfile'
import CartProvider from "./cartComponents/store/cart_provider";

function App() {
  return (
    <Router>
      <CartProvider>
        <AuthProvider>
          <Switch>
                <Route exact path="/signup" component={SignUp}></Route>
                <Route exact path="/login" component={Login}></Route>
                <Route exact path="/forgotPassWord" component={ForgotPassWord}></Route>
                <PrivateRoute exact path="/" component={Dhasborad}></PrivateRoute>
                <PrivateRoute exact path="/updateProfile" component={UpdateProfile}></PrivateRoute>
          </Switch>
        </AuthProvider>
      </CartProvider>
    </Router>
  );
}
export default App;
