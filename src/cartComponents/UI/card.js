

import "./card.css";
const Card = (prop) => {
  return <div className="card">{prop.children}</div>;
};

export default Card;