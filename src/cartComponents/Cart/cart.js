import { React, useContext, useState } from "react";
import Modal from "react-bootstrap/Modal";
import Button from "react-bootstrap/Button";
import "../mealsummary.css";
import CartContext from "../store/cart-context";
import Card from "../UI/card";
import CartForm from "./cartForm";

const Cart = (prop) => {
  const cartctx = useContext(CartContext);
  const [order, setOrder] = useState(false);
  const [defaultval, setdefaultval] = useState("Your Cart is empty");

  const totalAmount = cartctx.totalAmount.toFixed(2);
  const orderHandeler = (e) => {
    e.preventDefault();
    order ? setOrder(false) : setOrder(true);
  };


  const oncheckoutHandler = (user) => {
    try {
      fetch("https://food-order-721ed-default-rtdb.firebaseio.com/order.json", {
        method: "post",
        body: JSON.stringify({
          users: user,
          orderItem: cartctx.items,
        }),
      });
      alert("request processed");
      cartctx.clearItems();
      order ? setOrder(false) : setOrder(true);
    } catch {
      alert("request can not processed");
    }
  };
  const removeallItem = () => {
    cartctx.clearItems();
  };

  return (
    <>
      <Modal show={prop.show} onHide={prop.action}>
        <Modal.Header closeButton></Modal.Header>
        <Modal.Body>
          {cartctx.items.map((item) => {
            const addTocarthandeler = () => {
              cartctx.addItem({
                id: item.id,
                name: item.name,
                amount: Number(1),
                price: item.price,
              });
            };
            const removecarthandeler = () => {
              cartctx.removeItem(item.id);
            };
           

            return (
              <div>
                <diV className="card-flex">
                  <li className="list">
                    <h3>{item.name}</h3>
                    <div className="price">
                      {`$${item.price}`}
                      <span> &times;{item.amount} </span>
                    </div>
                  </li>
                  <div>
                    <Button className="mx-2"onClick={() => {addTocarthandeler();}}>+ </Button>
                    <Button className="mx-2"onClick={() => {removecarthandeler() }} >-</Button>
                  </div>
                </diV>
                <div class="devicer"></div>
              </div>
            );
          })}

        </Modal.Body>
                  <div className=" card-flex  total p-2 m-2  border border-info">
                        <div className="text-1 text-start">Total</div>
                        <div className="text-2 text-center">${totalAmount}</div>
                  </div>
                  {order && (<CartForm onOrder={orderHandeler} onCheckout={oncheckoutHandler} /> )}
        <Modal.Footer>
                    <Button variant="secondary" onClick={removeallItem} >Clear All </Button>
                    {cartctx.items.length > 0 && ( <Button variant="secondary" onClick={orderHandeler}>order</Button>)}
        </Modal.Footer>
      </Modal>
    </>
  );
};

export default Cart;
