import React from 'react'
import { Form,Button,Card,Alert } from 'react-bootstrap'
import { useRef ,useContext,useState} from 'react';

export default function CartForm(props) {
   const emailRef = useRef();
   const nameRef = useRef();
   const pincodeRef = useRef();
   const addressRef = useRef();
   const phoneNumberRef = useRef();
   const [error,setError]=useState("");

  
const conformHandeler = (e)=>{
  e.preventDefault();
  const enterName = nameRef.current.value;
  const enterEmail = emailRef.current.value;
  const enterAddress = addressRef.current.value;
  const enterNum = phoneNumberRef.current.value;
  const enterPincode = pincodeRef.current.value;

  props.onCheckout({
    name:enterName,
    email:enterEmail,
    address:enterAddress,
    phoneNumber:enterNum,
    picode:enterPincode
  })

}


   

    return (
      <>
        <Card  className=" w-100 m-auto">
          <Card.Body>
            {error && <Alert>{error}</Alert>}
            <Form className="scroll-form" onSubmit={conformHandeler}>
              <Form.Group className="mb-3" id="Email">
                <Form.Label>Name</Form.Label>
                <Form.Control type="text"  ref={nameRef} placeholder="Enter Name" />
              </Form.Group>
              <Form.Group className="mb-3" id="Email">
                <Form.Label>Email</Form.Label>
                <Form.Control type="email"  ref={emailRef} placeholder="Enter Email" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="passWord">
                <Form.Label>Address</Form.Label>
                <Form.Control type="text"  ref={addressRef} placeholder="Enter Address" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="confirmPassWord">
                <Form.Label>Pincode</Form.Label>
                <Form.Control type="number" ref={pincodeRef} placeholder="Enter Pincode" />
              </Form.Group>
              <Form.Group className="mb-3" controlId="confirmPassWord">
                <Form.Label>Phone Number</Form.Label>
                <Form.Control type="text" ref={phoneNumberRef} placeholder="Phone Number" />
              </Form.Group>
              <Button  type="submit" className="m-2" variant="primary" type="submit">conform</Button>
              <Button  type="button"  variant="primary" onClick={props.onOrder} >cancel</Button>
            </Form>
          </Card.Body>
        </Card>
      </>
    );
}