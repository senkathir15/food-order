import CartContext from "./cart-context"
import {useReducer } from "react";

const defaultSate = {
  item: [],
  totalAmount: 0,
  price: 1,
};
const cartReducer = (state, action) => {
  if (action.type === "add") {
    const existingCartIndex = state.item.findIndex((item) => item.id === action.payload.id);
    const existingCartItem = state.item[existingCartIndex];
    let updateNewItem;
    let updatedItems;
    if (existingCartItem) {
      updateNewItem = {
        ...existingCartItem,
        amount:
          Number(state.item[existingCartIndex].amount) +
          Number(action.payload.amount),
      };
      updatedItems = [...state.item];
      updatedItems[existingCartIndex] = updateNewItem;
    } else {
      updatedItems = state.item.concat(action.payload);
      console.log(updatedItems);
    }

    const upadteAmount =
      Number(state.totalAmount) +
      Number(action.payload.price * action.payload.amount);
    return {
      item: updatedItems,
      totalAmount: upadteAmount,
    };
  }

  if (action.type === "remove") {
    const existingCartIndex = state.item.findIndex((item) => item.id === action.payload);
    const existingCartItem = state.item[existingCartIndex];
    let updatedItems;
    const totalAmount = state.totalAmount - existingCartItem.price;

    if (existingCartItem.amount === 1) {
      updatedItems = state.item.filter((item) => item.id !== action.payload);
    } else {
      const updatedItem = {...existingCartItem,amount: existingCartItem.amount - 1,};
      console.log(updatedItem);
      updatedItems = [...state.item];
      console.log(updatedItems);
      updatedItems[existingCartIndex] = updatedItem;
      console.log(updatedItems);
    }
    return {
      item: updatedItems,
      totalAmount: totalAmount,
    };
  }
  if (action.type === "clear") {
    return {
      item: [],
      totalAmount: 0,
    };
  }

  return defaultSate;
};

const CartProvider = (props)=>{
    const [state,dispatch]=useReducer(cartReducer,defaultSate);
    const additemToCart =(item)=>{
        dispatch({type:"add", payload:item})
    }
    const removeitemFromCart =(id)=>{
        dispatch({type:"remove", payload:id})
    }
    const removeallItem =()=>{
        dispatch({type:"clear"})
    }
    const context = {
        items:state.item,
        totalAmount:state.totalAmount,
        addItem:additemToCart,
        removeItem:removeitemFromCart,
        clearItems:removeallItem
    };

    return (
            <CartContext.Provider value={context}>
                {props.children}
            </CartContext.Provider>
    );

}


export default CartProvider;